banner_area:
  subtitle: Orbot - Tor VPN for Smartphones
  title: Keep Apps Safe
  app_store_url: https://play.google.com/store/apps/details?id=org.torproject.android
  app_store_button: /assets/img/en-play-badge.png
  apple_url: https://apps.apple.com/us/app/orbot/id1609461599
  apple_button: /assets/img/apple.png
  fdroid_url: https://guardianproject.info/fdroid
  fdroid_button: /assets/img/fdroid.png
  image: /assets/img/LandingPage.png


promo_area:
  promo_area_items:
    - title: Traffic Privacy
      text: Encrypted traffic from any app, through the Tor network, gives you the highest standard of security and privacy.
      image: /assets/img/Browsing.png
      alt: Browsing-icon

    - title: Stop Snooping
      text: No extra eyes know what apps you are using, and when, or can stop you from using them.
      image: /assets/img/NoExtras.png
      alt: NoExtras-icon

    - title: Protected History
      text: No central logging of your traffic history or IP address by your network operator and app servers.
      image: /assets/img/TossHistory.png
      alt: TossHistory-icon


power_area:
  title: Powered by Tor
  text: Orbot is your trusted connection for Tor on Android and iOS.
  tor_image: /assets/img/Tor.png
  link_text: Learn more >
  link_url: about


detail_area:
  detail_area_items:
    - title: Protect and unblock<br/>specific apps.
      text: Orbot (on Android) allows you to specifically choose which apps to route through Tor, allowing you to still use apps and service that may be concerned with traffic coming from Tor.
      image: /assets/img/AppChoice.jpg
      alt: AppChoice-image

    - title: Super fast, super secure access to popular onion services.
      text: <a href="https://support.torproject.org/onionservices/" target="_blank">Onion services</a> are versions of sites that can only be accessed by Tor. Orbot allows any app to connect to an onion service, and for your phone to host an onion service itself!
      image: /assets/img/OnionSites.jpg
      alt: OnionSites-mage

    - title: Stay Connected
      text: Orbot offers access to a variety of Tor Bridges, so you can stay connected even on the most restricted networks
      image: /assets/img/Bridges.jpg
      alt: Bridges-image


featured_area:
  title: Featured in
  featured_area_items:
    - image: /assets/img/NewYorkTimes.png
      alt: NewYorkTimes-logo
      url: "http://www.nytimes.com/2014/02/20/technology/personaltech/privacy-please-tools-to-shield-your-smartphone-from-snoopers.html"

    - image: /assets/img/ArsTechnica.jpg
      alt: ArsTechnica-logo
      url: "https://arstechnica.com/information-technology/2012/02/from-encryption-to-darknets-as-governments-snoop-activists-fight-back/"

    - image: /assets/img/TechCrunch.png
      alt: TechCrunch-logo
      url: "https://techcrunch.com/2016/01/20/facebook-expands-tor-support-to-android-orbot-proxy/"

    - image: /assets/img/BoingBoing.png
      alt: BoingBoing-logo
      url: "https://boingboing.net/2018/10/23/hardware-orbot.html"

    - image: /assets/img/LifeHacker.png
      alt: LifeHacker-logo
      url: "https://lifehacker.com/the-apps-that-protect-you-against-verizons-mobile-track-1679936224"

    - image: /assets/img/Gizmodo.png
      alt: Gizmodo-logo
      url: "https://gizmodo.com/6-apps-to-secure-your-smartphone-better-1791777911"


special_area:
  title: “Orbot is the safest way to stay connected to the apps I need!"
  text: Play store review - May 21, 2018


dev_area:
  title: About the developers.
  text: Orbot is <a href="https://github.com/guardianproject/orbot/">free and open source</a>. Anyone may help contribute, audit or inspect the code. Main contributing programmers include <a href="https://github.com/n8fr8">Nathan Freitas</a>, <a href="https://gitlab.com/eighthave">Hans-Christoph Steiner</a>, <a href="https://github.com/bitmold">Bim</a>, <a href="https://die.netzarchitekten.com/">Benjamin Erhart</a> and more from <a href="https://guardianproject.info/">Guardian Project</a>.
  github_url: https://github.com/guardianproject/orbot
  github_label: Follow @GuardianProject on GitHub
  github_text: Follow @GuardianProject
